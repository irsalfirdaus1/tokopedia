package com.tokopedia.filter.view.model

data class ShopModel(
    val id: Int,
    val name: String,
    val city: String,
    var checked: Boolean = false
)
