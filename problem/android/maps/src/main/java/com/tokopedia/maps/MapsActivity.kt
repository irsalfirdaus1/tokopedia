package com.tokopedia.maps

import android.os.Bundle
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tokopedia.maps.model.CountryModel
import com.tokopedia.maps.service.NetworkError
import com.tokopedia.maps.service.NetworkProvider.provideApiService
import com.tokopedia.maps.service.isOnline
import com.tokopedia.maps.service.noNetworkConnectivityError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*


open class MapsActivity : AppCompatActivity() {
    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null
    private val compositeDisposable = CompositeDisposable()
    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)

        bindViews()
        initListeners()
        loadMap()
    }

    private fun bindViews() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
    }

    private fun initListeners() {
        buttonSubmit!!.setOnClickListener {
            editText ?: return@setOnClickListener

            val country = editText.text.toString()

            //validate input
            if (country.isEmpty()) {
                showDialog(resources.getString(R.string.please_enter_coutry))
                return@setOnClickListener
            }

            searchCountry(editText.text.toString())
        }
    }

    private fun searchCountry(country: String) {
        progressDialog.show()
        if (this.isOnline()) {
            val apiService = provideApiService()
            compositeDisposable.add(apiService.getCountryByName(country)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleSuccessCountry, this::handlerErrorCountry))
        } else {
            dismissProgressDialog()
            this.noNetworkConnectivityError()
        }
    }

    private fun handleSuccessCountry(list: List<CountryModel>) {
        dismissProgressDialog()
        if (list.isEmpty()) return

        val countryModel = list[0]
        setDataView(countryModel)
        pinToMap(countryModel)
    }

    private fun handlerErrorCountry(error: Throwable) {
        dismissProgressDialog()
        val networkError = NetworkError(error)

        showDialog(networkError.appErrorMessage)
    }

    private fun setDataView(countryModel: CountryModel) {
        val callingCodes = countryModel.callingCodes
        val callingCode = if (callingCodes.isNotEmpty()) {
            callingCodes[0]
        } else {
            ""
        }

        layoutSummary.visibility = VISIBLE
        txtCountryName.text = resources.getString(R.string.country_name, countryModel.nativeName)
        txtCountryCapital.text = resources.getString(R.string.capital, countryModel.capital)
        txtCountryPopulation.text = resources.getString(R.string.populations, countryModel.population.toString())
        txtCountryCallCode.text = resources.getString(R.string.calling_code, callingCode)
    }

    private fun pinToMap(countryModel: CountryModel) {
        googleMap ?: return

        val latLng = LatLng(countryModel.latlng[0], countryModel.latlng[1])

        googleMap!!.clear()
        googleMap!!.addMarker(MarkerOptions()
                .position(latLng)
                .title(countryModel.nativeName)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_position)))
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 4f))
    }

    fun loadMap() {
        mapFragment!!.getMapAsync { googleMap -> this@MapsActivity.googleMap = googleMap }
    }

    private fun showDialog(message: String?) {
        message ?: return

        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(message)
        dialog.setPositiveButton(R.string.confirm_ok) { dialogInterface, _ ->
            dialogInterface.dismiss()
        }
        dialog.show()
    }

    private fun dismissProgressDialog() {
        if (progressDialog.isShowing) {
            progressDialog.dismiss()
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}
