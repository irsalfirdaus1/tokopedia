package com.tokopedia.maps.service

data class ApiErrorModel(val message: String)