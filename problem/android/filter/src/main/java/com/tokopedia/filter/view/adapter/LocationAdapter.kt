package com.tokopedia.filter.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.ProductModel
import com.tokopedia.filter.view.model.ShopModel

class LocationAdapter(
        private val shopModels: List<ProductModel>,
        private val action: (String) -> Unit
) : RecyclerView.Adapter<LocationViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.location_item, parent, false)
        return LocationViewHolder(view)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val shopModel = shopModels[position]
        holder.bind(shopModel.shop, action)
    }

    override fun getItemCount(): Int = shopModels.size
}

