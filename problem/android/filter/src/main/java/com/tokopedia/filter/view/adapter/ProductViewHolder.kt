package com.tokopedia.filter.view.adapter

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tokopedia.core.currencyFormat
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.ProductModel
import kotlinx.android.synthetic.main.product_item.view.*

class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(productModel: ProductModel) {
        //set image with glide
        Glide.with(itemView.context)
                .load(productModel.imageUrl)
                .placeholder(ContextCompat.getDrawable(itemView.context, R.drawable.ic_camera))
                .into(itemView.productImage)

        itemView.productName.text = productModel.name
        itemView.productPrice.text = currencyFormat(productModel.priceInt.toDouble())
        itemView.productLocation.text = productModel.shop.city

        //set icon favorite click event
        itemView.icFavorite.setOnClickListener { onFavoriteClick(productModel) }
    }

    private fun onFavoriteClick(productModel: ProductModel) {
        //update favorite to model
        productModel.favorite = !productModel.favorite

        val colorRes = if (productModel.favorite) R.color.favorite else R.color.divider
        itemView.icFavorite.setColorFilter(ContextCompat.getColor(itemView.context, colorRes)) //update color
    }
}