package com.tokopedia.filter.view

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.tokopedia.filter.R
import com.tokopedia.filter.view.adapter.ProductAdapter
import com.tokopedia.filter.view.model.ProductModel
import com.tokopedia.filter.view.model.ProductResponseModel
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {
    private var productModels = ArrayList<ProductModel>()
    private lateinit var productAdapter: ProductAdapter
    private lateinit var filterSheetDialog: FilterDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        getProductList()
        setUpView()
        setUpProductAdapter()
    }

    private fun setUpView() {
        setUpDialogFilter()

        //set event
        fab.setOnClickListener {
            filterSheetDialog.show()
        }
    }

    private fun setUpDialogFilter() {
        filterSheetDialog = FilterDialog(this, productModels) { min, max, location ->
            productAdapter.filter(min, max, location)
            textEmptyProduct.visibility = if (productAdapter.itemCount == 0) VISIBLE else GONE
        }
    }


    private fun setUpProductAdapter() {
        productAdapter = ProductAdapter(productModels)
        productList.layoutManager = GridLayoutManager(this, 2)
        productList.adapter = productAdapter
    }

    /**
     * convert json product into data model
     */
    private fun getProductList() {
        //get raw json
        val json = resources.openRawResource(R.raw.products)
                .bufferedReader().use { it.readText() }

        if (json.isNotEmpty()) {
            //convert json to data model
            val productDataModel = Gson().fromJson(json, ProductResponseModel::class.java)
            productModels = productDataModel.data.products
        }
    }
}
