package com.tokopedia.filter.view.adapter

import androidx.recyclerview.widget.DiffUtil
import com.tokopedia.filter.view.model.ProductModel

class ProductDiffCallback(
        private val mOldProductModels: List<ProductModel>,
        private val mNewProductModels: List<ProductModel>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return mOldProductModels.size
    }

    override fun getNewListSize(): Int {
        return mNewProductModels.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return mOldProductModels[oldItemPosition].id == mNewProductModels[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldProduct = mOldProductModels[oldItemPosition]
        val newProduct = mNewProductModels[newItemPosition]

        return oldProduct.name == newProduct.name
    }
}