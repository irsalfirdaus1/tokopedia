package com.tokopedia.filter.view

import android.content.Context
import android.view.View
import androidx.core.view.ViewCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.tokopedia.core.currencyFormat
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.ProductModel
import kotlinx.android.synthetic.main.filter_dialog.*

class FilterDialog(
        context: Context,
        private val productModels: ArrayList<ProductModel>,
        private val action: (min: Double, max: Double, location: ArrayList<String>) -> Unit
) : BottomSheetDialog(context) {
    private var min: Double = 0.0
    private var max: Double = 0.0
    private var minPrice: Float? = null
    private var maxPrice: Float? = null
    private lateinit var frequentLocation: Map<String, Int>
    private var locationSelected: ArrayList<String> = ArrayList()
    private var filterLocationDialog: FilterLocationDialog

    init {

        val filterSheetView = View.inflate(context, R.layout.filter_dialog, null)
        setContentView(filterSheetView)

        setUpData()
        setUpChip()
        setUpRangeSlider()

        filterLocationDialog = FilterLocationDialog(context, productModels) { checkIsSelectedChip(it) }

        buttonFilter.setOnClickListener {
            action(min, max, locationSelected)
            this.dismiss()
        }
        textReset.setOnClickListener {
            locationSelected.clear()
            chipLocationGroup.clearCheck()
            setUpRangeSliderValue()
        }
    }

    private fun setUpData() {
        //generate location frequent
        frequentLocation = productModels.groupingBy { it.shop.city }
                .eachCount()
                .toList()
                .sortedByDescending { (_, v) -> v }
                .take(2)
                .toMap()

        //get min max value
        minPrice = productModels.minBy { it.priceInt }?.priceInt?.toFloat()
        maxPrice = productModels.maxBy { it.priceInt }?.priceInt?.toFloat()

    }

    private fun setUpChip() {
        setUpFrequentChipItem()
    }

    private fun checkIsSelectedChip(location: String) {
        if (frequentLocation.contains(location)) {
            if (!locationSelected.contains(location)) {
                locationSelected.clear()
                locationSelected.add(location)
                setUpFrequentChipItem()
            }
        } else {
            //remove view in index 0 when locations is more than 3
            if (chipLocationGroup.childCount > 3) {
                chipLocationGroup.removeViewAt(0)
            }

            locationSelected.clear()
            locationSelected.add(location)
            addChipItem(location, true)
        }
    }

    private fun setUpFrequentChipItem() {
        //reset view/clear
        chipLocationGroup.removeAllViews()

        for (location in frequentLocation) {
            //add chip for each frequent location
            addChipItem(location.key, false)
        }

        //add other chip
        addChipItem(context.resources.getString(R.string.other), false)
    }

    private fun addChipItem(location: String, first: Boolean) {
        val isOther = location == context.resources.getString(R.string.other)

        //set up chip item
        val chipLocationItem = layoutInflater.inflate(R.layout.chip_location_item, chipLocationGroup, false) as Chip
        chipLocationItem.text = location
        chipLocationItem.id = ViewCompat.generateViewId()
        chipLocationItem.isChecked = locationSelected.contains(location) //set chip selected
        chipLocationItem.isCheckable = !isOther //disable checked for other chip
        chipLocationItem.isClickable = true

        if (isOther) {
            //if chip is other
            //then set on click listener to show location filter
            chipLocationItem.setOnClickListener { showLocationDialog() }
        } else {
            //if chip is not other
            //set checked change lister
            chipLocationItem.setOnCheckedChangeListener { _, checked ->
                if (checked) {
                    locationSelected.add(location)
                } else {
                    locationSelected.remove(location)
                }
            }
        }

        //for add first chip when location selected from not frequent location
        if (first) {
            chipLocationGroup.addView(chipLocationItem, 0)
            return
        }
        chipLocationGroup.addView(chipLocationItem)
    }

    private fun showLocationDialog() {
        filterLocationDialog.show()
    }

    private fun setUpRangeSlider() {
        priceSlider.setLabelFormatter { value ->
            currencyFormat(value.toDouble())
        }

        setUpRangeSliderValue()

        priceSlider.addOnChangeListener { slider, _, _ ->
            //set selected min max
            min = slider.values[0].toDouble()
            max = slider.values[1].toDouble()

            //set min max to edittext
            editTextMinPrice.setText(currencyFormat(min))
            editTextMaxPrice.setText(currencyFormat(max))
        }
    }

    private fun setUpRangeSliderValue() {
        //set value to slider
        priceSlider.values = arrayListOf(minPrice, maxPrice)
        if (minPrice != null) {
            priceSlider.valueFrom = minPrice!!
            min = minPrice!!.toDouble()
            editTextMinPrice.setText(currencyFormat(min))
        }
        if (maxPrice != null) {
            priceSlider.valueTo = maxPrice!!
            max = maxPrice!!.toDouble()
            editTextMaxPrice.setText(currencyFormat(max))
        }
    }
}