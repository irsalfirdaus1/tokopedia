package com.tokopedia.filter.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.ProductModel

class ProductAdapter(
        private val productModels: ArrayList<ProductModel>
) : RecyclerView.Adapter<ProductViewHolder>() {
    private var productFilteredModels: ArrayList<ProductModel> = ArrayList()

    init {
        productFilteredModels.addAll(productModels)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.product_item, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val productModel = productFilteredModels[position]
        holder.bind(productModel)
    }

    override fun getItemCount(): Int = productFilteredModels.size


    fun updateProductListItems(productModels: List<ProductModel>) {
        val diffCallback = ProductDiffCallback(this.productFilteredModels, productModels)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        this.productFilteredModels.clear()
        this.productFilteredModels.addAll(productModels)
        diffResult.dispatchUpdatesTo(this)
    }

    fun filter(min: Double, max: Double, location: ArrayList<String>) {
        val filteredModel: List<ProductModel> = productModels.filter {
            var validMin = true
            var validMax = true
            var validLocation = true

            if (it.priceInt < min) {
                validMin = false
            }
            if (it.priceInt > max) {
                validMax = false
            }
            if (!location.isNullOrEmpty()) {
                if (location.size > 0) {
                    if (it.shop.city != location[0]) {
                        validLocation = false
                    }
                }
            }

            validMin && validMax && validLocation
        }

        updateProductListItems(filteredModel)
    }
}

