package com.tokopedia.filter.view

import android.content.Context
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tokopedia.filter.R
import com.tokopedia.filter.view.adapter.LocationAdapter
import com.tokopedia.filter.view.model.ProductModel
import kotlinx.android.synthetic.main.filter_location_dialog.*

class FilterLocationDialog(
        context: Context,
        private val productModels: ArrayList<ProductModel>,
        private val action: (String) -> Unit
) : BottomSheetDialog(context) {

    private lateinit var locationAdapter: LocationAdapter

    init {
        val locationSheetView = View.inflate(context, R.layout.filter_location_dialog, null)
        setContentView(locationSheetView)

        setUpAdapter()
    }

    private fun setUpAdapter() {
        locationAdapter = LocationAdapter(productModels.distinctBy { it.shop.city }) {
            action(it)
            this.dismiss()
        }
        locationList.adapter = locationAdapter
    }
}