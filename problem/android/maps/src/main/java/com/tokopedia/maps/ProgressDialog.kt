package com.tokopedia.maps

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.WindowManager

class ProgressDialog(context: Context) : Dialog(context, R.style.TransparentDialogTheme) {

    init {
        window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        val view = View.inflate(
                context,
                R.layout.dialog_progress,
                null
        )

        setContentView(view)
    }
}