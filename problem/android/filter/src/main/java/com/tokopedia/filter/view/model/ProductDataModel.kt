package com.tokopedia.filter.view.model

data class ProductDataModel(
    val products: ArrayList<ProductModel>
)