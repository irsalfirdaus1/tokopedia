package com.tokopedia.oilreservoir


/**
 * Created by fwidjaja on 2019-09-24.
 */
object Solution {
    fun collectOil(height: IntArray): Int {
        if (height.size <= 2) return 0

        var lMax = height[0]
        var rMax = height[height.size - 1]
        var maxWater = 0

        var l = 1
        var r: Int = height.size - 2

        while (l <= r) {
            if (lMax < rMax) {
                if (height[l] > lMax) lMax = height[l] else maxWater += lMax - height[l]
                l++
            } else {
                if (height[r] > rMax) rMax = height[r] else maxWater += rMax - height[r]
                r--
            }
        }

        return maxWater
    }
}
