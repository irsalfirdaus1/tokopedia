package com.tokopedia.filter.view.model

data class ProductModel(
        val id: Int,
        val name: String,
        val imageUrl: String,
        val priceInt: Int,
        val discountPercentage: Int,
        val slashedPriceInt: Int,
        var favorite: Boolean = false,
        val shop: ShopModel
)

