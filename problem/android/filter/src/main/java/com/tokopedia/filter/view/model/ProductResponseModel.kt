package com.tokopedia.filter.view.model

data class ProductResponseModel(
    val data: ProductDataModel
)