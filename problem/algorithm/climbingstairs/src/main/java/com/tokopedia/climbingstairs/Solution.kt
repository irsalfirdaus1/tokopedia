package com.tokopedia.climbingstairs

object Solution {
    fun climbStairs(n: Int): Long {
        val step = LongArray(n + 1)
        step[0] = 1
        step[1] = 1
        for (i in 2..n) {
            step[i] = step[i - 1] + step[i - 2]
        }

        return step[n]
    }
}
