package com.tokopedia.maps.model

data class CountryModel(
    val name: String,
    val nativeName: String,
    val capital: String,
    val population: Long,
    val callingCodes: List<String>,
    val altSpellings: List<String>,
    val region: String,
    val latlng: List<Double>

)