package com.tokopedia.minimumpathsum

import kotlin.math.min

object Solution {
    fun minimumPathSum(matrix: Array<IntArray>): Int {
        val minimum: Array<IntArray> = Array(matrix.size) { IntArray(matrix[0].size) }
        for ((i, col) in minimum.withIndex()) {
            for (j in col.indices) {
                minimum[i][j] += matrix[i][j]
                if (i > 0 && j > 0) {
                    minimum[i][j] += min(minimum[i - 1][j], minimum[i][j - 1])
                } else if (i > 0) {
                    minimum[i][j] += minimum[i - 1][j]
                } else if (j > 0) {
                    minimum[i][j] += minimum[i][j - 1]
                }
            }
        }
        return minimum[minimum.size - 1][minimum[0].size - 1]
    }

}
