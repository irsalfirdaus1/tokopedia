package com.tokopedia.filter.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tokopedia.filter.view.model.ShopModel
import kotlinx.android.synthetic.main.location_item.view.*

class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(shopModel: ShopModel, action: (String) -> Unit) {
        itemView.locationName.text = shopModel.city
        itemView.locationItemLayout.setOnClickListener {
            action(shopModel.city)
        }
    }
}