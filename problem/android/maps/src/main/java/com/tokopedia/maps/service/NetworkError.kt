package com.tokopedia.maps.service

import android.text.TextUtils
import com.google.gson.Gson
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.HttpURLConnection

open class NetworkError(private val error: Throwable?) : Throwable(error) {
    override val message: String
        get() = error!!.message!!
    val isAuthFailure: Boolean
        get() = error is HttpException &&
                error.code() == HttpURLConnection.HTTP_UNAUTHORIZED
    val responseCode: Int
        get() = (error as HttpException?)!!.code()
    val isResponseNull: Boolean
        get() = error is HttpException && error.response() == null
    val appErrorMessage: String?
        get() {
            if (error is IOException) return NETWORK_ERROR_MESSAGE
            if (error !is HttpException) return DEFAULT_ERROR_MESSAGE
            val response = (error as HttpException?)!!.response()
            if (response != null) {
                val message = getJsonStringFromResponse(response)
                if (!TextUtils.isEmpty(message)) {
                    return message
                }
                val headers = response.headers().toMultimap()
                if (headers.containsKey(ERROR_MESSAGE_HEADER)) return headers[ERROR_MESSAGE_HEADER]!![0]
            }
            return DEFAULT_ERROR_MESSAGE
        }

    fun getJsonStringFromResponse(response: Response<*>): String? {
        return try {
            val jsonString = response.errorBody()!!.string()
            val errorResponse = Gson().fromJson(jsonString, ApiErrorModel::class.java)
            errorResponse.message
        } catch (e: Exception) {
            null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as NetworkError
        return if (error != null) error == that.error else that.error == null
    }

    override fun hashCode(): Int {
        return error?.hashCode() ?: 0
    }

    companion object {
        const val DEFAULT_ERROR_MESSAGE = "Something went wrong, please try again"
        const val NETWORK_ERROR_MESSAGE = "Failed. Please check your connection"
        private const val ERROR_MESSAGE_HEADER = "Error-Message"
    }
}