package com.tokopedia.maps.service

import com.tokopedia.maps.model.CountryModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("/name/{name}")
    fun getCountryByName(
            @Path("name") name: String
    ): Observable<List<CountryModel>>


}